# -∗- coding:Utf-8 -∗-
from ina219 import INA219 , DeviceRangeError 
from time import sleep 
import time 
SHUNT_OHMS = 0.1 
#MAX_EXPECTED_AMPS = 2.0 
MAX_EXPECTED_AMPS = 0.3 
ina = INA219(SHUNT_OHMS , MAX_EXPECTED_AMPS) 
#ina.configure(ina.RANGE_16V , ina.GAIN_1_40MV) 
ina.configure(ina.RANGE_16V ) 
max_power_R_mW = 230
marchearret = ''
intervalle_temps = 0.5
temps0 = time.time()
temps = 0
def read_ina219(): 
	global temps ,intervalle_temps 
	try : 
		print ('Bus Voltage : {0:0.3f}V'.format(ina.voltage())) 
		print ('Bus Current : {0:0.3f}mA'.format(ina.current())) 
		if ina.power() < max_power_R_mW : 
			print('Power : {0:0.3f}mW'.format(ina.power ())) 
		else : 
			print('!!!! Dépassement de puissance !!!! Coupez tout !')
		print('Shunt Voltage : {0:0.3f}mV\n'.format(ina.shunt_voltage ()))
		fichier = open("condensateur.txt", "a") 
		fichier.write('{0:0.3f}'.format(temps))
		fichier.write(' {0:0.3f}'.format(ina.voltage())) 
		fichier.write(' {0:0.3f}'.format(ina.current())) 
		iv = ina.voltage()*0.005 
		fichier.write(' {0:0.3f}'.format(iv)) 
		ia = ina.current()*0.005 
		fichier.write(' {0:0.3f}\n'.format(ia)) 
		fichier.close()
		temps = time.time() - temps0
		print(temps)
	except DeviceRangeError as e: 
		print(e) 

while marchearret != 'q': 
	read_ina219() 
	sleep(intervalle_temps) #en secondes
