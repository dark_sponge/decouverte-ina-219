# -∗- coding:Utf-8 -∗-
from ina219 import INA219 , DeviceRangeError 
from time import sleep 
SHUNT_OHMS = 0.1 
#MAX_EXPECTED_AMPS = 2.0 
MAX_EXPECTED_AMPS = 0.3 
ina = INA219(SHUNT_OHMS , MAX_EXPECTED_AMPS) 
#ina.configure(ina.RANGE_16V , ina.GAIN_1_40MV) 
ina.configure(ina.RANGE_16V ) 
max_power_R_mW = 230
marchearret = '' 
def read_ina219(): 
	try : 
		print ('Bus Voltage : {0:0.3f}V'.format(ina.voltage())) 
		print ('Bus Current : {0:0.3f}mA'.format(ina.current())) 
		if ina.power() < max_power_R_mW : 
			print('Power : {0:0.3f}mW'.format(ina.power ())) 
		else : 
			print('!!!! Dépassement de puissance !!!! Coupez tout !')
		print('Shunt Voltage : {0:0.3f}mV\n'.format(ina.shunt_voltage ()))
		fichier = open("condensateur.txt", "a") 
		fichier.write('{0:0.3f}'.format(ina.voltage())) 
		fichier.write(' {0:0.3f}'.format(ina.current())) 
		iv = ina.voltage()*0.005 
		fichier.write(' {0:0.3f}'.format(iv)) 
		ia = ina.current()*0.005 
		fichier.write(' {0:0.3f}\n'.format(ia)) 
		fichier.close()
	except DeviceRangeError as e: 
		print(e) 
max_power_R_mW = 250 #int(input('Entrez la puissance max. de la résistace en mW : '))
while marchearret != 'q': 
	#marchearret = input('Tapez ENTER pour enregistrer une mesure , q pour quitter et n pour une nouvelle série de mesure : ')
	read_ina219() 
	if marchearret == 'n': 
		max_power_R_mW = 250 #int(input('Entrez la puissance max de la résistace en mW : '))
		fichier = open("condensateur.txt", "a") 
		fichier.write('\n\n') 
		fichier.close()
